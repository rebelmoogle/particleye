﻿using UnityEngine;
using System.Collections;

public class LightenUp : MonoBehaviour {

    MeshRenderer thisRenderer;
	// Use this for initialization
	void Start () 
    {
        thisRenderer = this.GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnParticleCollision(GameObject other)
    {
        // make it visible and glowy or something
        if (thisRenderer)
            thisRenderer.enabled = true;

    }
}
