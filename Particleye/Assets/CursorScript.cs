﻿using UnityEngine;
using System.Collections;

public class CursorScript : MonoBehaviour {
	public bool cursorShouldBeLocked;
	// Use this for initialization
	void Start () {
	}

	void Update()
	{
		if (cursorShouldBeLocked)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		else
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}

}
