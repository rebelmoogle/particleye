﻿using UnityEngine;
using System.Collections;
using System.IO;

public class CaptureStuff : MonoBehaviour {

    bool isCapturing;
    int screenNumber;

    // Use this for initialization
	void Start () 
    {
        isCapturing = false;
        screenNumber = 0;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetButton("Capture"))
        {
            Debug.Log("Capture state switched");
            isCapturing = !isCapturing;
        }

        if (isCapturing)
        {
            //Debug.Log("Capturing in progress");
            ScreenCapture.CaptureScreenshot("Screenshot" + screenNumber + ".png");
            screenNumber++;
        }
	}
}
