﻿using UnityEngine;
using System.Collections;

public class PlaySounds : MonoBehaviour {

    AudioSource[] shotSoundSources;
    [SerializeField]
    private AudioClip ShootSound; // this plays on each shot
    [SerializeField]
    private AudioClip CastSound; //this plays continuously when casting

    [SerializeField]
    private float fadeOutVal = 1.0f;

    float castFadeOut;
    float initVolume;


	// Use this for initialization
	void Start () 
    {
        HandInputHandler playerInput = this.gameObject.GetComponent<HandInputHandler>();
        if (playerInput != null)
        {
            playerInput.CastSparkles += player_CastSparkles;
            playerInput.CastRelease += player_CastRelease;
            playerInput.ShootSparkles += player_ShootSparkles;
        }

        shotSoundSources = this.GetComponents<AudioSource>();
        if (shotSoundSources.Length <= 0 || shotSoundSources.Length < 2)
        {
            Debug.LogError("FAILED to find Audiosources for shooting and casting particles, we need at least two. initializing default sources");
            shotSoundSources = new AudioSource[2];
        }

        shotSoundSources[0].clip = ShootSound;
        shotSoundSources[0].loop = false;
        shotSoundSources[1].clip = CastSound;

        initVolume = shotSoundSources[1].volume;
        castFadeOut = fadeOutVal;
	
	}

    private void player_ShootSparkles(GameObject e, float strength)
    {
        shotSoundSources[0].Play();
    }

    private void player_CastRelease(GameObject e)
    {
        castFadeOut = fadeOutVal;
    }

    private void player_CastSparkles(GameObject e, float strength)
    {
        shotSoundSources[1].volume = initVolume;
        if(!shotSoundSources[1].isPlaying)
        {
            shotSoundSources[1].loop = true;
            shotSoundSources[1].Play();
        }

    }
	
	// Update is called once per frame
	void Update () {
	    if(shotSoundSources[1].isPlaying && castFadeOut > 0.0f)
        {
            castFadeOut -= Time.deltaTime;
            Debug.Log("Cast Fade Out = " + castFadeOut);
            if(castFadeOut <= 0.0f)
            {
                shotSoundSources[1].Pause();
            }
            else
            {
                shotSoundSources[1].volume *= castFadeOut / fadeOutVal;
                Debug.Log("cast sound volume = " + shotSoundSources[1].volume);
            }
        }
	}
}
